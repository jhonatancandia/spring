package com.spring.data.springdatarest.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.spring.data.springdatarest.models.entity.User;
import com.spring.data.springdatarest.models.repository.UserRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Jhonatan Candia Romero
 */
@SynchronousExecution
public class UserSearchByIdCommand implements BusinessLogicCommand {

    @Getter
    private User user;

    @Setter
    private Long userId;

    @Autowired
    private UserRepository userRepository;

    @Override
    public void execute() {
        user = userRepository.findUserById(userId);
    }
}

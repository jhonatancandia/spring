package com.spring.data.springdatarest.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.spring.data.springdatarest.input.UserCreateInput;
import com.spring.data.springdatarest.models.entity.Role;
import com.spring.data.springdatarest.models.entity.User;
import com.spring.data.springdatarest.models.repository.UserRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Jhonatan Candia Romero
 */
@SynchronousExecution
public class UserCreateCommand implements BusinessLogicCommand {

    @Getter
    private User user;

    @Setter
    private UserCreateInput input;

    @Setter
    private String added;

    @Autowired
    private UserRepository repository;

    @Override
    public void execute() {
        user = repository.save(composeUser());
    }

    private User composeUser() {
        User instanceUser = new User();
        instanceUser.setUsername(input.getUsername());
        instanceUser.setPassword(input.getPassword());
        instanceUser.setName(input.getName());
        instanceUser.setLastName(input.getLastName());
        instanceUser.setEmail(input.getEmail());
        instanceUser.setAdded(added);

        return instanceUser;
    }


}

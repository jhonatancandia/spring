package com.spring.data.springdatarest.services;

import com.spring.data.springdatarest.framework.ServiceTransactional;
import com.spring.data.springdatarest.input.UserCreateInput;
import com.spring.data.springdatarest.models.entity.User;
import com.spring.data.springdatarest.models.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Jhonatan Candia Romero
 */
@ServiceTransactional
public class UserCreateServices {

    @Autowired
    private UserRepository userRepository;


    public User createUserEntity(UserCreateInput userCreateInput) {
        User userCompose = composeEntityUser(userCreateInput);

        return userRepository.save(userCompose);
    }

    private User composeEntityUser(UserCreateInput userCreateInput) {
        User user = new User();

        user.setEmail(userCreateInput.getEmail());
        user.setLastName(userCreateInput.getLastName());
        user.setName(userCreateInput.getName());
        user.setPassword(userCreateInput.getPassword());
        user.setUsername(userCreateInput.getUsername());

        return user;
    }
}

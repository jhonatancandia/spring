package com.spring.data.springdatarest.framework;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author Jhonatan Candia Romero
 */
@Service
@Transactional
@Scope("prototype")
@Retention(RetentionPolicy.RUNTIME)
public @interface ServiceTransactional {
}

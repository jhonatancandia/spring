package com.spring.data.springdatarest.controllers;

import com.spring.data.springdatarest.command.UserSearchByIdCommand;
import com.spring.data.springdatarest.models.entity.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author Jhonatan Candia Romero
 */
@RestController
@RequestMapping(value = "/user")
@Api(
        tags = "Search user by id"
)
public class UserSearchByIdController {

    @Autowired
    private UserSearchByIdCommand command;

    @ApiOperation(value = "Metodo para buscar un usuario segun su id")
    @RequestMapping(method = RequestMethod.GET, value = "{id}")
    public User getUserById(@PathVariable("id") Long id) {
        command.setUserId(id);
        command.execute();

        return command.getUser();
    }
}


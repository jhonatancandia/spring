package com.spring.data.springdatarest.controllers;

import com.spring.data.springdatarest.input.UserCreateInput;
import com.spring.data.springdatarest.models.entity.User;
import com.spring.data.springdatarest.services.UserCreateServices;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author Jhonatan Candia Romero
 */
@RestController
@RequestMapping(value = "/user")
@Api(
        tags = "Create user controller"
)
public class UserCreateController {

    @Autowired
    private UserCreateServices userCreateServices;

    @ApiOperation(value = "Metodo para crear un nuevo usuario")
    @RequestMapping(method = RequestMethod.POST)
    public User createEntityUser(@RequestBody UserCreateInput input){
        return userCreateServices.createUserEntity(input);
    }

}

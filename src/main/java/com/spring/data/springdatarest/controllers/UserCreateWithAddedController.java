package com.spring.data.springdatarest.controllers;

import com.spring.data.springdatarest.command.UserCreateCommand;
import com.spring.data.springdatarest.input.UserCreateInput;
import com.spring.data.springdatarest.models.entity.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author Jhonatan Candia Romero
 */
@RestController
@RequestMapping(value = "/user")
@Api(
        tags = "Create user controller with role"
)
public class UserCreateWithAddedController {

    @Autowired
    private UserCreateCommand userCreateCommand;

    @ApiOperation(
            value = "Create user with added"
    )
    @RequestMapping(method = RequestMethod.POST, value = "/added")
    public User createUserWithAdded(@RequestBody UserCreateInput input,
                                   @RequestParam("added") String added) {
        userCreateCommand.setInput(input);
        userCreateCommand.setAdded(added);
        userCreateCommand.execute();

        return userCreateCommand.getUser();
    }

}

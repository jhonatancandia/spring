package com.spring.data.springdatarest.models.repository;

import com.spring.data.springdatarest.models.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author Jhonatan Candia Romero
 */
public interface UserRepository extends JpaRepository<User, Long> {

    @Query("SELECT user FROM User user WHERE user.id = :id")
    User findUserById(@Param("id") Long id);
}

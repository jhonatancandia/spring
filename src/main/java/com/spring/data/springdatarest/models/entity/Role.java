package com.spring.data.springdatarest.models.entity;

/**
 * @author Jhonatan Candia Romero
 */
public enum Role {
    ADMIN,
    EMPLOYEE
}

package com.spring.data.springdatarest.input;

import lombok.Getter;

/**
 * @author Jhonatan Candia Romero
 */
@Getter
public class UserCreateInput {
    private String username;
    private String password;
    private String email;
    private String name;
    private String lastName;
}
